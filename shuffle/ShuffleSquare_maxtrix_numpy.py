import numpy as np
def check_equality(repr_u, offset_v, n,  M):
    if not len(repr_u) ==  n - offset_v:
        return False
    c = 0
    for a in repr_u:
        if not M[(a, offset_v+c)]:
            return False
        c += 1
    return True
def check_trivially_false(**kwargs):
    offset_v = kwargs['offset_v']
    n = kwargs['global_size']
    k = kwargs['left_hand_size']
    
    if k >= n - offset_v:
        return True
    #counteru = kwargs['lett_count_u']
    #counterv = kwargs['lett_count_v']
    #if np.min(counterv-counteru) <= 0 or np.max((counterv+counteru)%2) >= 1:
    #    return True
    return False

def _shuffle_square_matrix(**kwargs):
    repr_u = kwargs['repr_u']
    hash_repr_u = kwargs['hash_repr_u']
    offset_v = kwargs['offset_v']
    M = kwargs['global_matrix']
    n = kwargs['global_size']
    k = kwargs['left_hand_size']
    cache = kwargs['cache']
    ind_repr = kwargs['index_repr']
    verbose= kwargs['verbose']
    if verbose and verbose!='Cache':
        word = kwargs['word']
        u = ''.join(list(map(lambda e: word[e] if e in repr_u else '', range(n))))
        v = word[offset_v:]
        print(u,v, sep='\t')
        print(hash_repr_u, offset_v)
    if offset_v >= n :
        return False
    if (hash_repr_u, offset_v) in cache:
        if verbose:
            print('Cache hit')
        return cache[(repr_u,offset_v)]    
    if check_equality(repr_u, offset_v, n, M):
        cache[(repr_u, offset_v)] = True
        return True
    if check_trivially_false(**kwargs):
        cache[(repr_u, offset_v)] = False                
        return False
    res = False
    if  k > 0 and  M[(repr_u[0], offset_v)]:
        d = dict(kwargs)
        d['offset_v'] += 1
        d['repr_u'] = repr_u[1:]
        d['hash_repr_u'] = hash_repr_u[1:]
        d['left_hand_size'] += -1        
        res = _shuffle_square_matrix(**d)
    if not res:
        res = False
        d = dict(kwargs)
        d['offset_v'] += 1
        d['repr_u'] += (offset_v,)
        d['hash_repr_u'] += (ind_repr[offset_v],)
        d['left_hand_size'] += 1        
        res = _shuffle_square_matrix(**d)        
    cache[(repr_u,offset_v)] = res
    return res
    
        
def shuffle_square(word, verbose=False, cache=None):
    n = len(word)
    M = np.zeros((n,n), dtype='bool')
    lett_repr = {}  # map letter to indices (indice representing a letter) 
    lett_count = [] # list counting the number of occurence of each letter 
    repr_normal = {} # map letter representing an indices to its index in lett_count
    ind_repr = {} # dictionnary mapping postion to its representant with the same letter
    nb_lett = 0 # counter of number letters 
    for i in range(n):
        if word[i] not in lett_repr:
            lett_repr[word[i]] = i # the letter word[i] is represented by index i
            ind_repr[i] = i # i represent itself
            repr_normal[i] = nb_lett # the index of i in lett_count is the number of leter since so far
            lett_count.append(1) # we add a new entry in lett_count
            nb_lett += 1 
        else:
            r = lett_repr[word[i]]  # we get the representing index
            ind_repr[i] = r #  we set the representen to i to r 
            lett_count[repr_normal[r]] += 1 

        for j in range(i+1, n):
            b = (word[i]==word[j])
            M[(i,j)] = M[(j,i)] = b
            
    if cache is None:
        cache = dict()
    kwargs = {'word':word}
    kwargs['repr_u'] = tuple()
    kwargs['hash_repr_u'] = tuple()
    kwargs['offset_v'] = 0
    kwargs['global_matrix'] = M
    kwargs['global_size'] = n
    kwargs['left_hand_size'] = 0
    kwargs['cache'] = cache
    kwargs['index_repr'] = ind_repr  
    kwargs['verbose'] = verbose
    kwargs['lett_count_v'] = np.array(lett_count, dtype=int)
    kwargs['lett_count_u'] = np.zeros(nb_lett)
    kwargs['repr_normal'] = repr_normal
    return _shuffle_square_matrix(**kwargs)
