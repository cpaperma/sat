/* 
We explore accessibility to pairs of the shape (u,u) where u is a string in the DAG defined as follows: 

For a pairs (au, wav) -> (uw,v)  where |uw| <= |v|. 
Simple implementation in Python can be found in the file shuffle_pure.py 


To improve: 
* indexing each position to provides the next position of the same type in O(1). 
* normalizing once for all at the begining the word uv by looking of the order of letters
appearing the later.

*/

#include<unordered_map>
#include<set>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include <iostream>

using namespace std;
// maximal alphabet size
#define MAXA 64
bool permutability=true;
bool verbose=false;
string prefixpop="";
bool store_prefix_pop=true;
bool active_cache_prefix=true;
unordered_map<string, bool> cache;
unordered_map<string, bool> cache_prefix;

bool isShufflePairs(string u, string v)
{
  int k = u.length();
  int n = v.length();
  if (verbose)
    {
      cout << prefixpop <<'\t'<< u << '\t' << v<<'\n' ;
    }
  if (k > n)
    return false;
  if (k == 0 && k < n)
    return isShufflePairs(v.substr(0,1), v.substr(1));
  if (u == v)
    return true;
  char c = u[0];
  u = u.substr(1);
  for (int i=0;i<(n-k)/2+1;i++)
    {
     if (c == v[i])
       {
	 if (isShufflePairs(u + v.substr(0, i), v.substr(i+1)))
	  return true;
       }
    }
  return false;
}


bool isShuffleSquare(string word) {
  return isShufflePairs("", word);
}

int main(int argc, char **argv) {
  string u;
  for (int i=1;i<argc;i++)
    {
      char * arg = argv[i];
      if (arg[0] != '-')
	{
	  u = arg;
	}
      else
	{
	  if (strcmp(arg, "-verbose") == 0)
	    {
	      verbose=true;
	    }
	}
    }

  if (u.length() % 2 == 1)
    {
      cout << "False\n";
      return 0;
    }
  if (isShuffleSquare(u))
    {
    cout << "True\n";
    }
  else
    {
    cout << "False\n";
    }
  return 0;
}
