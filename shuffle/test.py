#!/usr/bin/python3 -O

# goal: finding long words on an alphabet which do not have any shuffle square
#  as factor
# cf Guegan and Ochem, A short proof that shuffle squares are 7-avoidable, 2016

from collections import deque
from string import ascii_lowercase
import sys

def unshuf(k, u, v, w):
    #print("try <%s> <%s> <%s>" % (u, v, w))
    if len(u) == 0:
        return len(v) == len(w)
    x = u[0]
    uu = u[1:]
    # print("%d %d" % (len(v), len(u)/2))
    if (len(v) < k and (len(v) >= len(w) or w[len(v)] == x)):
        if unshuf(k, uu, v + x, w):
            #print("won1")
            return True
    if (len(w) < k and (len(w) >= len(v) or v[len(w)] == x)):
        if unshuf(k, uu, v, w + x):
            #print("won2")
            return True
    return False

def naive(u):
    return unshuf(len(u)/2, u, "", "")

if __name__ == '__main__':
    w = sys.argv[1]
    print(testshufsquare(w))

