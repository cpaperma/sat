/* 
Trying to be smart by caching 
intermediate computation and exploring 
words up to permutation. 
We also try to avoid materializing string 
when offset can suffice. 

Representation of left and right words 
are simplified by using offset. 
A right words is simply an offset of the global 
word and the left words is a int array of 
consecutive offset position. 

The discarded prefix is also a fixed char.

Implementation to be done.
*/

#include<unordered_map>
#include<set>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include <iostream>
using namespace std;
// maximal alphabet size
#define MAXA 64
// maximal size of an input words
#define MAX_WORD 1000

bool permutability=true;
bool verbose=false;
string prefixpop="";
bool store_prefix_pop=false;
struct nodes
{
  string left; 
  string right;
  //  int count_left[MAXA];
  //int count_right[MAXA];
  bool operator==(const nodes &other) const
  {
    return (left == other.left && right ==other.right);
  }
};
namespace std
{
    template<> struct hash<nodes>
    {
      typedef nodes argument_type;
      typedef std::size_t result_type;
      std::size_t operator()(nodes const& n) const
      {
        std::size_t h1 = std::hash<std::string>{}(n.left);
        std::size_t h2 = std::hash<std::string>{}(n.right);
        return h1 ^ (h2 << 1); // or use boost::hash_combine (see Discussion)
      }
    };
}

  
unordered_map<nodes, bool> cache;

void permut(string v, char *p)
{
  int count = 0;
  for (int i=0; i<v.length(); i++){
    char c = v[i];
    if (p[c-'a'] == 0)
      {
	p[c-'a'] = count+'a';
	count++;
      }
  }
}
nodes construct_node(string u, string v)
{
  static nodes n = nodes();
  /*static int count_left[MAXA];
    static int count_right[MAXA];
  char p[MAXA]={0};
  permut(v, p);
  if (!permutability)
    {
      for (int i=0;i<MAXA;i++)
	{
	  p[i] = i+'a';
	}
    }
  n.left = "";
  for (char& c: u){
    n.left += p[c-'a'];
  }
  n.right = "";
  for (char& c: v){
    n.right += p[c-'a'];
    }*/
  n.left = u;
  n.right = v;
  return n;
}

bool triviallyShufflePairs(nodes n)
{
  if (n.left == n.right)
    {
      return true;
    }
  return false;
}
bool triviallyUnShufflePairs(nodes n)
{
  //cout << "Trivially False: "<<n.left<<":"<<n.right<<"\t";
  if (n.left.length() >= n.right.length())
    {
       //cout << " by size \n";
      return true;
    }
  // //cout << " Not by size ... ";
  // for (int i=0;i<MAXA;i++)
  //   {
  //     if (n.count_left[i]>0)
  // 	{
  // 	  if (n.count_left[i] > n.count_right[i])
  // 	    {
  // 	      cout << " by counting letter " << i<<"\n";
  // 	      return true;
  // 	    }
  // 	}
  //   }
  //cout << " ... no\n";
  return false;
}
bool isShufflePairsNodes(nodes n)
{
  bool res=false;
  
  if (verbose)
    {
      cout << prefixpop <<'\t'<< n.left << '\t' << n.right <<'\n';
    }
  if (cache.find(n) != cache.end()) {
    if (verbose)
      cout << "\t\t\tCache hit on "<<n.left<<":"<<n.right<<"\n";
    return cache[n];    
  }
  if (triviallyShufflePairs(n))
    {
      return true;
    }
  if (triviallyUnShufflePairs(n))
    {
      return false;
    }
  if (!res)
    {
      res = isShufflePairsNodes(construct_node(n.left + n.right[0], n.right.substr(1)));
    }
  if (!res && n.left[0] == n.right[0])
    {
      //cout << "First match \n";
      if (store_prefix_pop)
	prefixpop += n.left[0];
      res = isShufflePairsNodes(construct_node(n.left.substr(1), n.right.substr(1)));
      if (store_prefix_pop)
	prefixpop.pop_back();
    }
  
  cache[n] = res;
  return res;
}

bool isShufflePairs(string u, string v)
{
  return isShufflePairsNodes(construct_node(u, v));
}

bool isShuffleSquare(string word) {
    return isShufflePairs("", word);
}

int main(int argc, char **argv) {
  string u;
  for (int i=1;i<argc;i++)
    {
      char * arg = argv[i];
      if (arg[0] != '-')
	{
	  u = arg;
	}
      else
	{
	  if (strcmp(arg, "-verbose") == 0)
	    {
	      verbose=true;
	      store_prefix_pop=true;
	    }
	  if (strcmp(arg, "-unpermut") == 0)
	    permutability=false;
	  
	  if (strcmp(arg, "-prefix") == 0)
	    store_prefix_pop=true;
	}
    }
  
  if (isShuffleSquare(u))
    {
    cout << "True\n";
    }
  else
    {
    cout << "False\n";
    }
  return 0;
}

