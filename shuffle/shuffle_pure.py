
def shufflePairs(u,v, verbose=False): 
    if verbose: print(u, v, sep='\t')
    k = len(u)
    n = len(v)
    if u == v:
        return True
    if k > n: 
        return False
    if k == 0:
        return shufflePairs(v[0], v[1:], verbose=verbose)
    char = u[0]
    u = u[1:]
    for i in range(0, (n-k)//2+1):        
        if v[i] == char:
            res = shufflePairs(u+v[:i],v[i+1:], verbose=verbose)
            if res:
                return True
    return False
from sys import stdout

def shufflePairsCached(u, v, cache, verbose=False, call_depth=0, output=stdout):
    k = len(u)
    n = len(v)
    if verbose :
        print(call_depth, ":"*call_depth+' '*(20-call_depth), u+' '*(50-k), v+' '*(50-n), sep='\t', file=output)
    if u == v:
        return (True,  0)
    if k > n: 
        return (False, 0)
    if k == 0:
        return shufflePairsCached(v[0], v[1:], cache, verbose=verbose, output=output)    
    w = u+v
    if k >= cache.get(w, n):
        return (False, 1)
    
    cache_hit = 0
    char = u[0]
    u = u[1:]
    for i in range(0, (n-k)//2+1):        
        if v[i] == char:
            res, cache_hit2 = shufflePairsCached(u+v[:i],v[i+1:], cache, verbose=verbose, call_depth=call_depth+1, output=output)
            cache_hit += cache_hit2
            if res:
                return (True, cache_hit)
    cache[w] = min(k, cache.get(w,n))
    return (False,  cache_hit)
    
def isShuffleSquare(u, verbose=False, cached_version=False, output=stdout):
    if not cached_version:
        return shufflePairs("", u, verbose=verbose)
    else:
        res, cache_hit= shufflePairsCached("", u, dict(), verbose=verbose, output=output)
        if verbose:
            print('Cache hit', cache_hit)
            
        return res

