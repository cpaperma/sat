#!/usr/bin/python3

import sys

cache = {}

for f in sys.argv[1:]:
    with open(f, 'r') as fh:
        for l in fh.readlines():
            ff = l.strip().split(' ')
            w = ff[0]
            v = ff[1]
            if w in cache.keys():
                ov = cache[ff[0]] 
                if ov != v:
                    print("ERROR: %s is %s but was previously %s" % (w, v, ov))
                    sys.exit(1)
            else:
                cache[w] = v

