#!/usr/bin/python3
import numpy as np
import pycosat
import sys
import time
import subprocess
from collections import Counter
def _get_clauses(u):
    """
    Produce a CNF which is SAT iff u is a Shuffle Square
    """
    n = len(u)
    p = n//2
    if n % 2 == 1: return False,{}
    variables = [(label,i,j)
                 for label in ["y","nl2","nr1","nm"]
                 for i in range(n)
                 for j in range(i+1,n)]
    d = {variables[k]:k+1 for k in range(len(variables))}
    clauses = []
    for i in range(n):
        f = []
        for j in range(n):
            if i!=j:
                f.append(d[("y",min(i,j), max(i,j))])
        clauses.append(f)

    for i in range(n):
        for j in range(i+1,n):
            if j<n-1:
                f = [-d[("y",i,j)], d[("nl2", j,n-1)]]
                clauses.append(f)

            if j>0 and i<j-1:
                f = [-d[("y",i,j)], d[("nl2", i,j-1)]]
                clauses.append(f)
                f = [-d[("nl2",i,j)], d[("nl2",i,j-1)]]
                clauses.append(f)

            if i<n-1 and i<j-1:
                f = [-d[("y",i,j)], d[("nr1", i+1,j)]]
                clauses.append(f)
                f = [-d[("nr1",i,j)], d[("nr1",i+1,j)]]
                clauses.append(f)

            for lab in ["nl2","nr1"]:
                f = [-d[(lab,i,j)], -d[("y",i,j)]]
                clauses.append(f)

            if i<j-1:
                f = [-d[("y",i,j)], d[("nm",i+1,j)]]
                clauses.append(f)
                f = [-d[("nm",i,j)], -d[("y",i,j)]]
                clauses.append(f)
                f = [-d[("y",i,j)], d[("nm",i,j-1)]]
                clauses.append(f)
                f = [-d[("nm",i,j)], d[("nm",i+1,j)]]
                clauses.append(f)
                f = [-d[("nm",i,j)], d[("nm",i,j-1)]]
                clauses.append(f)
    for i in range(n):
        for j in range(i+1, n):
            if u[i] != u[j]:
                clauses.append([-d[("y",i,j)]])
    return clauses, variables

def _pycosat(u, solution=True):
    clauses,variables = _get_clauses(u)
    if not clauses:
        return False
    res = pycosat.solve(clauses)
    if res == "UNSAT": return False
    true_res = list(map(lambda e:(u[e[0]],u[e[1]], e), map(lambda e:variables[e-1][1:], filter(lambda e:e>0 and variables[e-1][0]=="y", res))))
    #print(list(map(lambda e:variables[e-1],filter(lambda e:variables[e-1][0]=="nr2" and e>0 and variables[e-1][1] == 4 , res))))
    l = list(map(lambda e:e[2][0], true_res))
    l.extend(map(lambda e:e[2][1], true_res))
    c = Counter(l)
    for i in c:
        if c[i] > 1:
            print("error", i)
    if solution:
        return true_res
    else:
        return True
def _glucose(u, glucose="../glucose-syrup-4.1/parallel/glucose-syrup"):
    clauses, variables = _get_clauses(u)
    if not clauses:
        return False
    my_run = subprocess.Popen([glucose, "-verb=0"],
                              stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    my_run.stdin.write("p cnf {} {}\n".format(len(variables), len(clauses)).encode())
    for c in clauses:
        my_run.stdin.write(((" ".join(list(map(lambda e:str(e),c)))+" 0\n").encode()))
    out, err = my_run.communicate()
    x = out.decode()
    if "UNSATISFIABLE" in x:
        return False
    if "SATISFIABLE" in x:
        return True
    print("error: could not parse result from glucose, trying again")
    time.sleep(1)
    return _glucose(u, glucose=glucose)

def _cpp_string(u, verbose=False):
    inputs = ['./naive.out', u]
    if verbose:
        inputs.append('-verbose')
    my_run = subprocess.Popen(inputs, stdout=subprocess.PIPE)
    out, err = my_run.communicate()
    res = out.decode().strip()    
    if res == 'True':
        return True
    return False

def _glucosesh(u, glucose="./glucose.sh"):
    clauses, variables = _get_clauses(u)
    if not clauses:
        return False
    with open("mytmpfile", 'w') as fhandle:
        fhandle.write("p cnf {} {}\n".format(len(variables), len(clauses)))
        for c in clauses:
            fhandle.write(((" ".join(list(map(lambda e:str(e),c)))+" 0\n")))
    my_run = subprocess.Popen([glucose, u],
                              stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    out, err = my_run.communicate()
    x = out.decode()
    if "UNSATISFIABLE" in x:
        return False
    if "SATISFIABLE" in x:
        return True
    print("error: could not parse result from glucose, trying again")
    time.sleep(1)
    return _glucosesh(u, glucose=glucose)

def _cadical(u, cadical="../cadical/build/cadical"):
    clauses, variables = _get_clauses(u)
    if not clauses:
        return False
    my_run = subprocess.Popen([cadical],
                              stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    my_run.stdin.write("p cnf {} {}\n".format(len(variables), len(clauses)).encode())
    for c in clauses:
        my_run.stdin.write(((" ".join(list(map(lambda e:str(e),c)))+" 0\n").encode()))
    out, err = my_run.communicate()
    out = list(filter(lambda e:len(e)>0 and e[0]=='s',out.decode().split("\n")))[0]
    if "UNSATISFIABLE" in out:
        return False
    return True
def word_to_DIMACS_str(u):
    clauses, variables = _get_clauses(u)
    if not clauses:
        return False
    s = """c
c Instance generated to solve the shuffle-square problem for 
c the word: "{}".
c details at: {}
c
"""
    s = s.format(u, "https://a3nm.net/work/research/questions/#words-without-shuffle-squares")
    s += "p cnf {} {}\n".format(len(variables), len(clauses))
    for c in clauses:
        s += " ".join(list(map(lambda e:str(e),c)))+" 0\n"
    return s
    


def load_example():
    return list(map(lambda e:(e.split()[0],e.split()[1]=='True'), open('cache_glucosesh').read().split('\n')[:-1]))

def test_with_examples(size=100, method='pure', verbose=False):
    from shuffle_pure import isShuffleSquare
    if method == 'pure':
        solve = lambda e: isShuffleSquare(e, cached_version=False)
    elif method == 'cached':
        solve = lambda e: isShuffleSquare(e, cached_version=True)
    elif method == 'cpp':
        solve = _cpp_string
    else:
        from shuffle import _pycosat as pycosolve
        solve = lambda e: pycosolve(e, solution=False)
    test = load_example()
    i = 0
    for word, result in test[:size]:
        if verbose: print('starting:', word)
        else:
            print(str(100*i/size)[:5],'%',i,' '*20, end='\r')
        res2 = solve(word)
        if result != res2:
            print("Error", word, result, res2)
        elif verbose:
            print("Ok", word, result)
        i+= 1



def solve(u, method="pycosat"):
    if method == "pycosat":
        return _pycosat(u)
    if method == "glucose":
        return _glucose(u)
    if method == "glucose.sh":
        return _glucosesh(u)
    if method == "cadical":
        return _cadical(u)

    raise NotImplemented("{} is not implemented".format(error))
if __name__ == "__main__":
    w = sys.argv[1]
    if len(sys.argv) == 2:
        method = "pycosat"
    if len(sys.argv) == 3:
        method = sys.argv[2]
    ret = solve(w, method=method)
    #ret = shuffle(w)
    if ret:
        print ("True")
    else:
        print("False")
