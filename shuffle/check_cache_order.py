#!/usr/bin/python3

import sys

cache = {}

fhl = []
for f in sys.argv[1:]:
    fhl.append(open(f, 'r'))
while True:
    seen = None
    for i in range(len(fhl)):
        fh = fhl[i]
        l = None
        l = fh.readline().strip()
        if l == "":
            l = None
        if l != None and seen == None:
            seen = l
        if l != None and l != seen:
            print("ERROR: read %s in %d but %s before" % (l, i, seen))
            sys.exit(1)
    if seen == None:
        break

