#! /usr/bin/python3
import numpy as np

def edges_to_vertices(list_edges):
    vertices = {i for e in list_edges for i in e}
    return list(vertices)
    
def bi_graph(list_edges1, list_edges2, clause_only=False):
    vertices1 = edges_to_vertices(list_edges1)
    vertices2 = edges_to_vertices(list_edges2)
    pairs = [(i,j) for i in vertices1 for j in vertices2]
    clauses = []
    for e in list_edges1:
        e = tuple(e)
        for k in vertices2:
            variable1 = pairs.index((e[0],k)) + 1
            variable2 = pairs.index((e[1],k)) + 1
            clauses.append((-variable1, -variable2))            
    for e in list_edges2:
        e = tuple(e)
        for k in vertices1:
            variable1 = pairs.index((k,e[0])) + 1
            variable2 = pairs.index((k,e[1])) + 1
            clauses.append((variable1,variable2))
    clauses = set(clauses)
    if clause_only:
        return clauses
    return solve(clauses)

def graph_to_adjency_matrix(list_edges):
    vertices = edges_to_vertices(list_edges)
    mat = np.zeros((len(vertices), len(vertices)), dtype=int)
    for e in list_edges:
        t = [vertices.index(tuple(e)[0]),vertices.index(tuple(e)[1])]
        mat[tuple(t)] = True
        t.reverse()
        mat[tuple(t)] = True
    return np.matrix(mat)

            
def triangles_check_4col(n):
    l = [(i,j,b) for i in range(n) for j in range(i+1,n) for b in [0,1]]
    clauses = []
    for i in range(n):
        for j in range(i+1, n):
            for k in range(j+1, n):
                P = [l.index((i,j,1))+1, l.index((i,k,1))+1, l.index((j,k,1))+1]
                M = [l.index((i,j,0))+1, l.index((i,k,0))+1, l.index((j,k,0))+1] 
                for p in [0,1]:      
                    for m in [0,1]:  
                        c = []
                        c.extend(map(lambda e:((-1)**p)*e, P))
                        c.extend(map(lambda e:((-1)**m)*e, M))
                        clauses.append(c)
    return clauses
def clauses_to_minisat(clauses):
    S = set()
    for c in clauses:
        S.update(set(map(lambda e:abs(e), c)))
    variables = len(S)
    clauses_nb = len(clauses)
    s =  "c generated automatically\n"
    s += "p cnf {} {}\n".format(variables, clauses_nb)
    for c in clauses:
        s += " ".join(map(lambda e:str(e),c))+" 0\n"
    return s

def solve_local(clauses, filename='tmp.minisat', nb_threads=4, nb_mem=20000):
    from os import popen
    with open(filename, "w") as f:
        f.write(clauses_to_minisat(clauses))
    print("Input done")
    print(popen("./solver {} -maxmemory={} -maxnbthreads={}".format(filename, nb_mem, nb_threads)).read())


def solve_local_triangles(n, nb_threads=4, nb_mem=20000):
    solve_local(triangles_check_4col(n),nb_threads=nb_threads, nb_mem=nb_mem)

if __name__ == "__main__":
    import sys
    if len(sys.argv) >= 3:
        nb_threads = int(sys.argv[2])
    else:
        nb_threads = 4
    if len(sys.argv) >= 4:
        nb_mem = int(sys.argv[3])
    else:
        nb_mem = 20000
    solve_local_triangles(int(sys.argv[1]), nb_threads=nb_threads, nb_mem=nb_mem)
