#!/bin/bash

# monitor progress from cache file
# read from cache file $1
# write to $2

tail -f "$1" | while read l; do date | tr '\n' ' '; echo $l; done | stdbuf -i0 -o0 tee horodatage
