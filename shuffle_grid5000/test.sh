#!/bin/bash

time (head -100 cache_naive | while read l; do W=$(echo $l | cut -d' ' -f1);
echo $W >/dev/stderr; ./shuffle.py "$W" $1 </dev/null; done > outputs)

diff outputs <(head -100 answers_naive)
