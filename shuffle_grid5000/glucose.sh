#!/bin/bash

# wrapper for glucose, which sometimes hangs

WORD="$2"

#echo "glucose.sh for $WORD" >/dev/stderr
# exec ../glucose-syrup-4.1/simp/glucose <tmps.clause 2>/dev/null
(../glucose-syrup-4.1/parallel/glucose-syrup -maxnbthreads=56 -maxmemory=20000 -fifosize=1000000 <mytmpfile >myoutfile) &
PID="$!"
sleep 0.1

#echo "starting to monitor" > /dev/stderr
while true
do
  LINE=$(top -n1 -d0.3 -b -p $PID | tail -1)
  #echo "$LINE" > /dev/stderr
  if echo "$LINE" | grep PID >/dev/null
  then
    # process has finished
    break
  else
    CPU=$(echo "$LINE" | awk '{print $9}')
    if (( $(echo "$CPU < 50" |bc -l) )); then
      #echo "CPU USAGE $CPU" >/dev/stderr
      #echo "GLUCOSE STUCK for $WORD, KILL IT" >/dev/stderr
      kill -9 $PID
      sleep 0.5
      #echo "TRY AGAIN" >/dev/stderr
      # and try again...
      exec ./glucose.sh $@
    fi
  fi
done

cat myoutfile

