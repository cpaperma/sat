#!/usr/bin/python3 -Ou

# goal: finding long words on an alphabet which do not have any shuffle square
#  as factor
# cf Guegan and Ochem, A short proof that shuffle squares are 7-avoidable, 2016

from collections import deque
from string import ascii_lowercase
from test import naive
from shuffle import solve
from random import shuffle
import sys

# argv1 = alphabet size
# argv2 = cache file (if defined)
# argv3 = method (if def)
# argv4 = random?

cache_filename = None
alphabet = list(ascii_lowercase)[:int(sys.argv[1])]
print(alphabet)
do_random = False

if len(sys.argv) > 2:
    cache_filename = sys.argv[2]
    if cache_filename == "none":
        cache_filename = None

method = "pycosat"

if len(sys.argv) > 3:
    method = sys.argv[3]

if len(sys.argv) > 4 and sys.argv[4] == 'true':
    do_random = True

print("method is %s" % method)
print("cache filename is %s" % cache_filename)

cache = {}

fcache = None

if cache_filename:
    # load cache
    myf = open(cache_filename, 'r')
    for l in myf.readlines():
        f = l.strip().split(' ')
        if f[1] == "False":
            cache[f[0]] = False
        else:
            cache[f[0]] = True
    myf.close()

    fcache = open(cache_filename, 'a')

def testshufsquare(u):
    if method == 'naive':
        return naive(u)
    if solve(u, method=method) == False:
        return False
    return True

def shufsquare(u):
    global cache
    #print(len(cache.keys()))
    global fcache
    if u in cache.keys():
        return cache[u]
    r = testshufsquare(u)
    #print("Cache miss: %s" % u)
    cache[u] = r
    if fcache:
        print(("%s %s" % (u, "True" if r else "False")), file=fcache)
        fcache.flush()
    return r

def shufsquarefct(x):
    counts = {}
    for a in alphabet:
        counts[a] = 0
    for i in reversed(range(len(x))):
        counts[x[i]] += 1
        ok = True
        for a in alphabet:
            if counts[a] % 2 != 0:
                ok = False
                break
        if ok:
            if shufsquare(x[i:]):
                return True
    return False

def explore(x):
    if shufsquarefct(x):
        return
    print(x)
    if do_random:
        shuffle(alphabet)
    for a in alphabet:
        explore(x + a)

explore("")

print("exploration terminated")

