#include<unordered_map>
#include<algorithm>
#include<random>
#include<cstdlib>
#include<vector>

// goal: finding long words on an alphabet which do not have any shuffle square
// as factor
// cf Guegan and Ochem, A short proof that shuffle squares are 7-avoidable, 2016
// tweak: random order (hardcodes alphabet size 4)

// for an alphabet of size 3 the code quickly shows there are no options
// for an alphabet of size 4, it's open...

// !!! maximal size of words being considered
#define MAX 1024

// maximal alphabet size
#define MAXA 26

std::minstd_rand0 engine(42);

int histo[MAXA]; // histogram to count letter occurrences
int histoV[MAXA]; // the same, while bruteforcing

using namespace std;

int asize; // alphabet size

// cache of what is a shuffle square
unordered_map<string, bool> cache;

// word U under consideration, and current decomposition V and W
char U[MAX], V[MAX];

// lenk = length of U/2; offset = where U actually begins
int lenk, offset;

// test if U+offset is a shuffle square
// i, j, k are current positions in U, V, W
// V must be the longest
bool unshuf(int i, int j, int k) {
  //printf("try %d %d %d offset %d lenk=%d\n", i, j, k, offset, lenk);
  if (i == 2*lenk) {
    // we have gone over all U
    return true;
  }
  if (i && j==k) return false; // suffixes were tested already

  int x = U[offset+i];
  // try allocate x to V...
  // only possible if V is not full
  // and if the number of occurrences of x in V is still OK
  // and if the new char in V matches the corresponding char in W
  //   (if it exists)
  if (histoV[x-'a'] < histo[x-'a']/2) {
    //printf("consider1\n");
    V[j] = x;
    histoV[x-'a']++;
    if (unshuf(i+1, j+1, k))
      return true;
    histoV[x-'a']--;
  }
  // try allocate x to W (i.e., do not take x)...
  // impose that |W| < |V| for this
  if ((k < j) && (V[k] == x)) {
    //printf("consider2 %d %d %d\n", i, j, k);
    // this is tail recursive
    return (unshuf(i+1, j, k+1));
  }
  // ... else fail
  return false;
}

// test if U+offset is a shuffle square (n = length of U)
// uses the cache
bool shufsquare(int n) {
  //printf("shufsquare %s with n=%d offset=%d\n", U, n, offset);
  // test if U+offset is a shuffle square (memoized)
  string str(U+offset);
  if (cache.find(str) != cache.end()) {
    //printf("cache hit\n");
    return cache[str];
  }
  string str2(str);
  reverse(str2.begin(), str2.end());
  if (cache.find(str2) != cache.end()) {
    //printf("cache hit\n");
    return cache[str2];
  }
  
  // use histograms to guide the bruteforce
  for (int i = 0; i < asize; i++)
    histoV[i] = 0;

  //printf("unshuffle %s with n=%d offset=%d\n", U, n, offset);
  bool v = unshuf(0, 0, 0);
  cache[str] = v;
  cache[str2] = v;
  return v;
}

// test if U has a suffix which is a shuffle square (n = length of U)
bool shufsquare2(int n) {
  // test if U or one of its suffixes is bad
  // testing suffixes from shortest to longest for performance
  //printf("consider %s %d\n", U, n);

  for (int i = 0; i < asize; i++)
    histo[i] = 0;

  for (int i=n-2; i>=0; i-=2) {
    offset = i;
    lenk = (n-i)/2;

    // counts of the number of occurrences of each letter in the suffix
    histo[U[i+1]-'a']++;
    histo[U[i]-'a']++;

    // optimization: a shuffle square must have an even number of occurrences
    // for each letter
    bool ok = true;
    for (int i = 0; i < asize; i++)
      if (histo[i] % 2) {
        ok = false;
        break;;
      }

    if (ok && shufsquare(n-i)) {
      //printf("square at %d!\n", i);
      return true;
    }
  }
  //printf("not square\n");
  return false;
}

// explore by DFS to find a word that avoids the shuffle squares
void explore(int x) {
  U[x] = 0;
  if (shufsquare2(x))
    return;
  printf("%s\n", U);
  vector<int> v({0,1,2,3}); // HARDCODING ALPHA SIZE
  shuffle(v.begin(), v.end(), engine); //seed
  for (int i = 0; i < asize; i++) {
    U[x] = 'a'+v[i];
    explore(x+1);
  }
}

int main(int argc, char **argv) {
  asize = atoi(argv[1]);
  explore(0);
  printf("FINISHED\n");
}
